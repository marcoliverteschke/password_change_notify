# Password Change Notify module

## What's this now???

Plain and simple, this module notifies Drupal users via e-mail whenever their password changes. If they did it themselves… cool. If they didn't… <a href="https://youtu.be/JiPcEoip6ZI" target="_blank">SEND FOR THE MAN</a>!

## But… how?

The module changes the user entity's password field to be a custom field item class. This class inherits everything from the vanilla password field, except that before any save operation, it compares the previous and new password values. If the values differ, an e-mail is sent to the user's stored address. If the current e-mail address does not match the previous one, the e-mail is sent to both addresses, just to be safe.

## To-Do

* [X] Overwrite password field
* [X] Compare current and previous password values
* [X] Trigger e-mail on mismatch
* [X] Add config variables for e-mail template
* [X] Use config variables in e-mail texts
* [X] Replace tokens in e-mails
* [ ] Add config form