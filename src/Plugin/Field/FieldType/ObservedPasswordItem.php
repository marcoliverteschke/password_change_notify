<?php

namespace Drupal\password_change_notify\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\PasswordItem;
use Drupal\user\UserInterface;

/**
 * Defines the 'observed_password' entity field type.
 *
 * @FieldType(
 *   id = "observed_password",
 *   label = @Translation("Password"),
 *   description = @Translation("An entity field containing a password value, with attached change notifications."),
 *   no_ui = TRUE,
 * )
 */
class ObservedPasswordItem extends PasswordItem {

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    if (isset($this->parent->parent->getEntity()->original) && $this->parent->parent->getEntity()->original instanceof UserInterface) {
      $currentUser = $this->parent->parent->getEntity();
      $originalUser = $this->parent->parent->getEntity()->original;
      $newPassword = $this->values['value'];
      $originalPassword = $originalUser->get('pass')->value;
      if (\Drupal::service('password')->check($newPassword, $originalPassword) === FALSE) {
        \Drupal::service('plugin.manager.mail')->mail('password_change_notify', 'user_notify', 'user@example.com', 'en', ['user' => $currentUser]);
      }
    }
    parent::preSave();
  }

}
